# HTML 5 base

Static [html5 boilerplate site](https://bitbucket.org/tymyrddin/html5-base/wiki/html5.md) for practising setting up and maintaining a pipeline from Bitbucket to a [Digital Ocean droplet](https://bitbucket.org/tymyrddin/html5-base/wiki/DO.md).
